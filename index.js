// cross, toes, empty
const undo = document.querySelector('.undo-btn');
const redo = document.querySelector('.redo-btn');
const restart = document.querySelector('.restart-btn');

const state = {
  queue: [],
  fields: [], //cross, toes, empty
  turn: 'cross',
  caret: 0,
  result: 'inProcess',
  resultData: {},
};

function setClassForCell({ cell, type, num }) {
  const classes = ['ch', 'r', 'win', 'horizontal', 'vertical', 'diagonal-left', 'diagonal-right'];
  cell.classList.remove(...classes);
  const MAP_CLASSES = {
    cross: 'ch',
    toes: 'r',
  };
  if (MAP_CLASSES[type]) {
    cell.classList.add(MAP_CLASSES[type]);
  }
  if (state.result !== 'inProcess' && state.resultData.cells && state.resultData.cells.includes(num)) {
    cell.classList.add('win');
    cell.classList.add(state.resultData.type);
  }
}

function renderGame() {
  state.fields.forEach((type, i) => {
    const cell = field.querySelector(`[data-id='${i}']`);
    setClassForCell({ cell, type, num: i });
  });
}

function renderButtons() {
  const { result, caret, queue } = state;
  if (result !== 'inProcess') {
    undo.setAttribute('disabled', true);
    redo.setAttribute('disabled', true);
    return;
  }
  if (caret > 0) {
    undo.removeAttribute('disabled');
  } else {
    undo.setAttribute('disabled', true);
  }
  if (caret < queue.length - 1) {
    redo.removeAttribute('disabled');
  } else {
    redo.setAttribute('disabled', true);
  }
}

function renderWonResult() {
  const MAP_WON_MESSAGE = {
    crossWon: 'Crosses won!',
    toesWon: 'Toes won!',
    draw: "It's a draw!",
  };
  const title = document.querySelector('.won-title');
  const message = document.querySelector('.won-message');
  if (state.result === 'inProcess') {
    title.classList.add('hidden');
  } else {
    title.classList.remove('hidden');
    message.textContent = MAP_WON_MESSAGE[state.result];
  }
}

function render() {
  renderGame();
  renderButtons();
  renderWonResult();
}

function checkCells(elems) {
  const { fields } = state;
  let temp = fields[elems[0]];
  for (let elem of elems) {
    elemVal = fields[elem];
    if (elemVal === 'empty' || elemVal !== temp) {
      return false;
    }
    temp = elemVal;
  }
  return temp;
}

function checkWin() {
  const horizontal = [];
  const vertical = [];
  const diagonalRight = [];
  const diagonalLeft = [];
  let resultCheckCells;
  for (let i = 0; i < ROWS_COUNT; i++) {
    for (let j = 0; j < COLS_COUNT; j++) {
      const val = j + i * COLS_COUNT;
      horizontal.push(val);
      if (i === 0) {
        vertical[j] = [];
      }
      vertical[j].push(val);
      if (i === j) {
        diagonalRight.push(val);
      }
      if (i + j === COLS_COUNT - 1) {
        diagonalLeft.push(val);
      }
    }
    resultCheckCells = checkCells(horizontal);
    if (resultCheckCells) {
      return {
        type: 'horizontal',
        who: resultCheckCells,
        cells: horizontal,
      };
    }
    horizontal.length = 0;
  }
  for (let arr of vertical) {
    resultCheckCells = checkCells(arr);
    if (resultCheckCells) {
      return {
        type: 'vertical',
        who: resultCheckCells,
        cells: arr,
      };
    }
  }
  resultCheckCells = checkCells(diagonalRight);
  if (resultCheckCells) {
    return {
      type: 'diagonal-right',
      who: resultCheckCells,
      cells: diagonalRight,
    };
  }
  resultCheckCells = checkCells(diagonalLeft);
  if (resultCheckCells) {
    return {
      type: 'diagonal-left',
      who: resultCheckCells,
      cells: diagonalLeft,
    };
  }
  if (!state.fields.includes('empty')) {
    return {
      who: 'draw',
    };
  }
  return false;
}

function pushHistory() {
  if (state.caret < state.queue.length - 1) {
    state.queue.splice(state.caret + 1);
  }
  state.queue.push({
    turn: state.turn,
    fields: state.fields.slice(),
  });
  state.caret = state.queue.length - 1;
}

function setGameStep(num) {
  const MAP_RESULTS = {
    cross: 'crossWon',
    toes: 'toesWon',
    draw: 'draw',
  };
  state.fields[num] = state.turn;
  state.turn = state.turn === 'cross' ? 'toes' : 'cross';
  const _result = checkWin();
  if (_result) {
    state.result = MAP_RESULTS[_result.who];
    state.resultData = _result;
  }
  pushHistory();
  setToStorage();
  render();
}

function setToStorage() {
  localStorage.setItem('state', JSON.stringify(state));
}

field.addEventListener('click', e => {
  if (!e.target.classList.contains('cell') || e.target.classList.contains('cr') || e.target.classList.contains('r')) {
    return;
  }
  const numCell = e.target.dataset.id;
  setGameStep(numCell);
});

undo.addEventListener('click', e => {
  if (state.queue.length < 2 || state.result !== 'inProcess') {
    return;
  }
  state.caret--;
  state.fields = state.queue[state.caret].fields.slice();
  state.turn = state.queue[state.caret].turn;
  setToStorage();
  render();
});

redo.addEventListener('click', e => {
  if (state.caret >= state.queue.length - 1 || state.result !== 'inProcess') {
    return;
  }
  state.caret++;
  state.fields = state.queue[state.caret].fields.slice();
  state.turn = state.queue[state.caret].turn;
  setToStorage();
  render();
});

restart.addEventListener('click', e => {
  if (state.result === 'inProcess') {
    return;
  }
  start();
});

window.addEventListener('storage', e => {
  const stateFromStorage = JSON.parse(localStorage.getItem('state'));
  if (stateFromStorage) {
    Object.keys(stateFromStorage).forEach(key => {
      state[key] = stateFromStorage[key];
    });
    render();
  }
});

function start() {
  state.fields = Array(ROWS_COUNT * COLS_COUNT).fill('empty');
  state.queue.length = 0;
  state.caret = 0;
  state.result = 'inProcess'; //'crossWon', 'toesWon', 'draw'
  state.resultData = null;
  state.turn = 'cross';
  pushHistory();
  setToStorage();
  render();
}

document.addEventListener('DOMContentLoaded', () => {
  const stateFromStorage = JSON.parse(localStorage.getItem('state'));
  if (stateFromStorage) {
    Object.keys(stateFromStorage).forEach(key => {
      state[key] = stateFromStorage[key];
    });
    render();
  } else {
    start();
  }
});
